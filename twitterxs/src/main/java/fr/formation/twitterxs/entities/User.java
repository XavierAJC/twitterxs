package fr.formation.twitterxs.entities;

import java.time.*;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table //On peut changer le nom de la table ici
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable=false, length=100)
	private String firstname;
	@Column(nullable=false, length=100)
	private String lastname;
	@Column(nullable=false, length=255, unique = true)
	private String email;
	@Column(nullable=false)
	private LocalDate birthDate;
	@Column(nullable=false, updatable=false)
	private LocalDateTime subscriptionDate;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable=false)
	private Region region;
	@Embedded //intégré
	private UserSecurity userSecurity;
	@OneToMany(mappedBy="author", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	private Set<Tweet> tweets;
	
	public User() {
		super();
	}

	public Integer getId() {
		return id;
	}
	public String getFirstname() {
		return firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public String getEmail() {
		return email;
	}
	public LocalDate getBirthDate() {
		return birthDate;
	}
	public LocalDateTime getSubscriptionDate() {
		return subscriptionDate;
	}
	public Region getRegion() {
		return region;
	}
	
	//le setId doit être private pour des raisons de sécurité et consistency
	private void setId(Integer id) { 
		this.id = id;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public void setSubscriptionDate(LocalDateTime subscriptionDate) {
		this.subscriptionDate = subscriptionDate;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public UserSecurity getUserSecurity() {
		return userSecurity;
	}

	public void setUserSecurity(UserSecurity security) {
		this.userSecurity = security;
	}

	public Set<Tweet> getTweets() {
		return tweets;
	}

	public void setTweets(Set<Tweet> tweets) {
		this.tweets = tweets;
	}
	
	/**
	 * Enumeration of roles.
	 * <p>
	 * Enumeration order is not meaningful.
	 */
	public enum Role {
		// The ROLE_USER role
		ROLE_USER("USER"),
		// The ROLE_ADMIN role
		ROLE_ADMIN("ADMIN"),
		// The ROLE_ACTUATOR role
		ROLE_ACTUATOR("ACTUATOR");
		
		private final String notPrefixed;

		private Role(String notPrefixed) {
			this.notPrefixed = notPrefixed;
		}
		
		/**
		 * Return the default role.
		 */
		public static Role getDefault() {
			return ROLE_USER;
		}
		
		public boolean isUser() {
			return equals(ROLE_USER);
		}
		
		public boolean isAdmin() {
			return equals(ROLE_ADMIN);
		}
		
		public boolean isActuator() {
			return equals(ROLE_ACTUATOR);
		}
	}
}
