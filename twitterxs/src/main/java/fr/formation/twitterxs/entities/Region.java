package fr.formation.twitterxs.entities;

import javax.persistence.*;

@Entity
public class Region {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable=false, length=4)
	private String language;
	@Column(nullable=false, length=2)
	private String country;
	
	public Region() {
		super();
	}
	
	public Region(String language, String country) {
		super();
		setLanguage(language);
		setCountry(country);
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		if (language.length() > 4)
			System.out.println("language doit comporter max 4 caractères");
		this.language = language;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		if (country.length() > 2)
			System.out.println("country doit comporter max 2 caractères");
		this.country = country;
	}
	

}
