package fr.formation.twitterxs.entities;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.*;


@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"author_id",
						"postDate"})) // le couple (id, postDate) est unique
public class Tweet {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false)
	private User author;
	
	@Column(nullable = false, updatable = false)
	private LocalDateTime postDate;
	
	@Column(nullable = false)
	private LocalDateTime editDate;
	
	@Column(nullable = false, length = 280)
	private String content;
	
	public Tweet() {
		super();
	}

	public Integer getId() {
		return id;
	}

	/**
	 * Keep private for security and consistency
	 */
	private void setId(Integer id) {
		this.id = id;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public LocalDateTime getPostDate() {
		return postDate;
	}

	public void setPostDate(LocalDateTime postDate) {
		this.postDate = postDate;
	}

	public LocalDateTime getEditDate() {
		return editDate;
	}

	public void setEditDate(LocalDateTime editDate) {
		this.editDate = editDate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * Indicates whether some other object is "equal to" this {@code user}.
	 * <p>
	 * Two {@code User} objects are considered equal if their {@code email} are equal case-sensitively.
	 * 
	 * @param an object to test equality against
	 * @return {@code true} if this {@code user} is the same as {@code obj};
	 * 			{@code false} otherwise
	 * @see String#equals(Object)Object)
	 */
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj) {
//			return true;
//		}
//		if (!(obj instanceof Tweet))
//			return false;
//		Tweet other = (Tweet) obj;
//		return author.equals(other.author) && postDate.equals(other.postDate);
//	}
	/**
	* Returns a hash code value for this {@code tweet}
	*/
//	@Override
//	public int hashCode() {
//		return Objects.hash(author, postDate);
//	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(author, postDate);
	}

	//Généré automatiquement fonctionne aussi
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Tweet)) {
			return false;
		}
		Tweet other = (Tweet) obj;
		return Objects.equals(author, other.author) && Objects.equals(postDate, other.postDate);
	}

	
}
