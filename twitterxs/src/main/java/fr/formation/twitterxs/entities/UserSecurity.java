package fr.formation.twitterxs.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import fr.formation.twitterxs.BooleanConverter;
import fr.formation.twitterxs.entities.User.Role;

@Embeddable //intégrable permet de délocalisé des champs
public class UserSecurity implements Serializable {
	
	private static final long serialVersionUID = 193956612997173231L;
	
	@Column(length = 50, nullable = false, unique = true, updatable = false)
	private String username;
	
	@Column(length = 100, nullable = false)
	private String password;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 13, nullable = false)
	private Role role = Role.getDefault();
	
	@Convert(converter = BooleanConverter.class)
	@Column(length = 1, nullable = false)
	private boolean accountNonExpired = true;
	
	@Convert(converter = BooleanConverter.class)
	@Column(length = 1, nullable = false)
	private boolean accountNonLocked = true;
	
	@Convert(converter = BooleanConverter.class)
	@Column(length = 1, nullable = false)
	private boolean credentialsNonExpired = true;
	
	@Convert(converter = BooleanConverter.class)
	@Column(length = 1, nullable = false)
	private boolean enabled = true;
	
	//Constructor
	protected UserSecurity() {
	}

	//Getters/setters
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	
}
