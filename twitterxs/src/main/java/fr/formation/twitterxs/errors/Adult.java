/**
 * 
 */
package fr.formation.twitterxs.errors;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE_PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Retention(RUNTIME)
@Target({ FIELD, ANNOTATION_TYPE, TYPE_PARAMETER })
@Constraint(validatedBy = { AdultValidator.class })
/**
 * @author Xavier
 * Validates a date againts majority
 */
public @interface Adult {
	
	int age() default 18;
	
	String message() default "{E_NOT_ADULT}";
	
	//Obligatoire pour le framework spring
	Class<?>[] groups() default {};
	
	Class<? extends Payload>[] payload() default {};

}
