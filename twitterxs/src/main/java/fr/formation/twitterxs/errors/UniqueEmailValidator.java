package fr.formation.twitterxs.errors;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import fr.formation.twitterxs.services.UserService;

public class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String> {
	
	@Autowired
	private UserService userService;

	@SuppressWarnings("unused")
	@Override
	public boolean isValid(String email, ConstraintValidatorContext context) {
		if (null == email || email.isEmpty())
			return true;
		return userService.uniqueEmail(email);
		
	}

}
