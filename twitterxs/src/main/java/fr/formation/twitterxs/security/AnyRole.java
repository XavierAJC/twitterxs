package fr.formation.twitterxs.security;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.security.access.annotation.Secured;

@Retention(RUNTIME)
@Target({ TYPE, METHOD })
@Secured({"ROLE_ACTUATOR", "ROLE_ADMIN", "ROLE_USER"}) //or
public @interface AnyRole {

}
