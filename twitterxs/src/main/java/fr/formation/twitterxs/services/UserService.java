package fr.formation.twitterxs.services;

import java.util.List;
import fr.formation.twitterxs.dto.UserCreateDto;
import fr.formation.twitterxs.dto.UserDto;
import fr.formation.twitterxs.entities.User;

public interface UserService {
	
	public List<UserDto> userList(String str);

	public void create(UserCreateDto dto);

	public void delete(Integer id);

	public boolean uniqueEmail(String email);

		
}
