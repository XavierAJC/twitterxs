package fr.formation.twitterxs.services.impl;

import org.springframework.stereotype.Service;
import fr.formation.twitterxs.services.TestService;

@Service  //Ne pas oublier dans l'implémentation
public class TestServiceImpl implements TestService {

	@Override
	public String getWeekday(String abbr) {
		String result = null;
		switch (abbr) {
		case "mo":
			result = "Monday";
			break;
		case "tu":
			result = "Tuesday";
			break;
			default:
				result = "Friday";
				break;
		}
		return result;
	}

}
