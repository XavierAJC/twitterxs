package fr.formation.twitterxs.services.impl;

import java.time.LocalDateTime;
import java.util.List;
import org.modelmapper.ModelMapper;
//import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import fr.formation.twitterxs.dto.UserCreateDto;
import fr.formation.twitterxs.dto.UserDto;
import fr.formation.twitterxs.entities.Region;
import fr.formation.twitterxs.entities.User;
import fr.formation.twitterxs.repository.RegionJpaRepository;
import fr.formation.twitterxs.repository.UserJpaRepository;
import fr.formation.twitterxs.services.UserService;

@Service
public class UserServiceImpl implements UserService {
		
	private final UserJpaRepository uJpaRepo;
	private final RegionJpaRepository rJpaRepo;
	private final PasswordEncoder encoder;
	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	public UserServiceImpl(UserJpaRepository uJpaRepo, RegionJpaRepository rJpaRepo,
			PasswordEncoder encoder) {
		super();
		this.uJpaRepo = uJpaRepo;
		this.rJpaRepo = rJpaRepo;
		this.encoder =  encoder;
	}

//	@Transactional //Si fetchType.LAZY
	@Override
	public List<UserDto> userList(String str) {
		return uJpaRepo.findAllAsDto(str);
	}

	@Override
	public void create(UserCreateDto dto) {
		//Version 1
		User user = mapper.map(dto, User.class); //Populate
		//Version 2
//		User entity = new User();
//		user.setBirthDate(dto.getBirthDate());
//		user.setEmail(dto.getEmail());
//		user.setFirstname(dto.getFirstname());
//		user.setLastname(dto.getLastname());
		user.setSubscriptionDate(LocalDateTime.now());
		
		//getOne returns only id-populated entity
		Region region = rJpaRepo.getOne(dto.getRegionId());
//		rJpaRepo.findById(dto.getRegionId()); // 2e methode  
		user.setRegion(region);
		
		String pwd = user.getUserSecurity().getPassword();
		String encoded = encoder.encode(pwd);
		user.getUserSecurity().setPassword(encoded);
		
		uJpaRepo.save(user); //nécessaire pour enregistrer l'entrée dans la base
	}

	@Override
	public void delete(Integer id) {
		uJpaRepo.deleteById(id);
		
	}

	@Override
	public boolean uniqueEmail(String email) {
		// TODO Auto-generated method stub
		return !uJpaRepo.existsByEmailIgnoreCase(email);
	}
	
	

}
