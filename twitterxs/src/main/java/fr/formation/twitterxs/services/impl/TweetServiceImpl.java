package fr.formation.twitterxs.services.impl;


import java.time.LocalDateTime;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.formation.twitterxs.dto.SearchResultDto;
import fr.formation.twitterxs.dto.TweetCreateDto;
import fr.formation.twitterxs.dto.TweetDto;
import fr.formation.twitterxs.dto.TweetSearchDto;
import fr.formation.twitterxs.entities.Tweet;
import fr.formation.twitterxs.entities.User;
import fr.formation.twitterxs.repository.TweetJpaRepository;
import fr.formation.twitterxs.repository.UserJpaRepository;
import fr.formation.twitterxs.services.TweetService;

@Service
public class TweetServiceImpl implements TweetService {
	
	@Autowired
	private ModelMapper mapper;
	private final TweetJpaRepository tweetJpaRepo;
	private final UserJpaRepository userJpaRepo;

	protected TweetServiceImpl(TweetJpaRepository tweetJpaRepo, 
			UserJpaRepository userJpaRepo) {
		this.tweetJpaRepo = tweetJpaRepo;
		this.userJpaRepo = userJpaRepo;
	}

	@Override
	public void create(TweetCreateDto dto) {

			Tweet tweet = mapper.map(dto, Tweet.class); //Populate
			tweet.setPostDate(LocalDateTime.now());
			tweet.setEditDate(LocalDateTime.now());
			User author = userJpaRepo.getOne(dto.getAuthorId());
			tweet.setAuthor(author);
			tweetJpaRepo.save(tweet); 


	}
	
	@Override
	public SearchResultDto<TweetDto> feed(TweetSearchDto dto) {
		// returns the number of tweets for a given user (liker)
		Pageable pageable = PageRequest.of(dto.getPage(), dto.getSize());
		// Returns a page of tweets for given username and pageable information
		Page<TweetDto> page = tweetJpaRepo.findByUsername(dto.getUsername(),
				pageable);
		return new SearchResultDto<>(page.getContent(), page.getTotalElements());
	}

}
