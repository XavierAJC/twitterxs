package fr.formation.twitterxs.services;

import fr.formation.twitterxs.security.Principal;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserDetailsService {
	
	public Principal loadUserByUsername (String username) throws UsernameNotFoundException;

}
