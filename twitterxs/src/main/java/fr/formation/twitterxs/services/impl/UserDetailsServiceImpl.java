package fr.formation.twitterxs.services.impl;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.formation.twitterxs.entities.User;
import fr.formation.twitterxs.repository.UserJpaRepository;
import fr.formation.twitterxs.security.Principal;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	private final UserJpaRepository userJpaRepo;
	
	
	// @Autowired not mandatory with one constructor
	protected UserDetailsServiceImpl(UserJpaRepository userJpaRepo) {
		super();
		this.userJpaRepo = userJpaRepo;
	}

	// Principal is a UserDetails => covariant return
	@Override
	public Principal loadUserByUsername(String username) 
			throws UsernameNotFoundException {
		User user = userJpaRepo.findByUserSecurityUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("No user found with username: " + username);
		}
		return new Principal(user);
	}

}
