package fr.formation.twitterxs.services;

import java.util.List;

import fr.formation.twitterxs.dto.ValueLabelDto;
import fr.formation.twitterxs.entities.Region;

public interface RegionService {
	
	public Integer save (Region region);
	
	public List<Region> regionList();
	
	public List<ValueLabelDto> findAll();
}
