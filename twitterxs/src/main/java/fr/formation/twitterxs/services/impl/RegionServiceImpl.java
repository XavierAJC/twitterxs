package fr.formation.twitterxs.services.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.formation.twitterxs.dto.ValueLabelDto;
import fr.formation.twitterxs.entities.Region;
import fr.formation.twitterxs.repository.RegionJpaRepository;
import fr.formation.twitterxs.repository.RegionRepository;
import fr.formation.twitterxs.services.RegionService;

@Service
public class RegionServiceImpl implements RegionService{
	
	private final RegionJpaRepository jpaRepo;
	
	@Autowired //On injecte le repository
	private RegionRepository repo;
	
	@Autowired
	public RegionServiceImpl(RegionJpaRepository jpaRepo) {
		super();
		this.jpaRepo = jpaRepo;
	}


	@Override
	public Integer save(Region region) {
		return repo.save(region);
	}

	@Override
	public List<Region> regionList() {
		return jpaRepo.findAll();
	}


	@Override
	public List<ValueLabelDto> findAll() {
		return jpaRepo.findAllAsDto();
	}

}
