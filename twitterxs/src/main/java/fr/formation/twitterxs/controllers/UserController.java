package fr.formation.twitterxs.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.twitterxs.dto.UserCreateDto;
import fr.formation.twitterxs.dto.UserDto;
import fr.formation.twitterxs.services.UserService;

@RestController
@RequestMapping("/user")
@PreAuthorize("hasRole('ROLE_USER')") // Meme chose que @Secured("ROLE_USER")
public class UserController extends BaseController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/list/{str}")
	protected List<UserDto> userList(@PathVariable("str") String str) {
		return userService.userList(str);
	}
	
	@PostMapping("/create")
	protected void create(@Valid @RequestBody UserCreateDto dto) {
		userService.create(dto);
	}
	
	
	@DeleteMapping("/delete/{id}")
	@Secured({"ROLE_ADMIN", "ROLE_ACTUATOR"})
	protected void delete(@PathVariable("id") Integer id) {
		userService.delete(id);
	}

}
