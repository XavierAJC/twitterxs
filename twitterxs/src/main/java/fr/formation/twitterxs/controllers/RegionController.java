package fr.formation.twitterxs.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import fr.formation.twitterxs.entities.Region;
import fr.formation.twitterxs.services.RegionService;

@RestController
@RequestMapping("/region")
public class RegionController extends BaseController {
	
	@Autowired
	private RegionService service; //champ du type de l'interface, pas de l'implémentation
	
	@PostMapping("/save")
	protected Integer save(@RequestBody Region region) {
		return service.save(region);
	}
	
	@GetMapping("/list")
	protected List<Region> regionList() {
		return service.regionList();
	}
	
	
	/** dans POSTMAN
	 * On envoie une requete POST
	 * avec:
	 * Header: Content-Type = application/json
	 * Body: 
	 * {
	 * "language":"fr",
	 * "country":"CA"
	 * }
	 * et ca créé l'entrée dans la BDD
	 */
	 

}
