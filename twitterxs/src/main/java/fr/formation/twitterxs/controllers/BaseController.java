package fr.formation.twitterxs.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import fr.formation.twitterxs.errors.ApiErrors;
import fr.formation.twitterxs.errors.MessageNotReadableError;
import fr.formation.twitterxs.errors.ValidationError;
import fr.formation.twitterxs.security.Principal;

public abstract class BaseController extends ResponseEntityExceptionHandler {

	public BaseController() {
		// TODO Auto-generated constructor stub
	}
	
	/**
     * Convenient method returning the request URI.
     *
     * @return the request URI
     */
    protected static String getRequestURI() {
    ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder
        .currentRequestAttributes();
    return attr.getRequest().getRequestURI();
    }
    
    @SuppressWarnings("unused")
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
    		MethodArgumentNotValidException ex, HttpHeaders headers,
    		HttpStatus status, WebRequest request) {
    	// Intercept field errors
    	List<FieldError> fieldErrors = (List<FieldError>) ex.getBindingResult().getFieldError();
    	List<ValidationError> errors = new ArrayList<>(fieldErrors.size());
    	ValidationError error = null;
    	for (FieldError fieldError : fieldErrors) {
			String entityName = fieldError.getObjectName();
			String fieldName = fieldError.getField();
			String errorCode = fieldError.getDefaultMessage();
			error = ValidationError.ofFieldType(entityName, fieldName, errorCode);
			errors.add(error);
		}
    	// Intercept global errors such as multi-fields errors
    	List<ObjectError> globalErrors = (List<ObjectError>) ex.getBindingResult().getGlobalError();
    	for (ObjectError globalError : globalErrors) {
			String entityName = globalError.getObjectName();
			String fieldName = globalError.getCode();
			String errorCode = globalError.getDefaultMessage();
			error = ValidationError.ofFieldType(entityName, fieldName, errorCode);
			errors.add(error); // Merge field and global errors
    	}
    	ApiErrors<ValidationError> apiErrors = new ApiErrors<>(errors, 
    			status.value(), getRequestURI());
    	return new ResponseEntity<>(apiErrors, status);
    }
    
    /**
     * Overriden to customize the response body in case of HTTP message not
     * readable.
     */
    @SuppressWarnings("unused")
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
    		HttpMessageNotReadableException ex, HttpHeaders headers,
    		HttpStatus status, WebRequest request) {
    	List<MessageNotReadableError> errors = new ArrayList<>();
    	errors.add(new MessageNotReadableError(ex.getMessage()));
    	ApiErrors<MessageNotReadableError> apiErrors = new ApiErrors<>(errors, 
    			status.value(), getRequestURI());
    	return new ResponseEntity<>(apiErrors, status);
    }
    
    /**
     * Returns the authenticated user (principal) identifier.
     *
     * @return the authenticated user identifier
     * @see Principal#getUserId()
     */
    protected static Integer getUserId() {
    return getPrincipal().getUserId();
    }

    protected static Principal getPrincipal() {
    return (Principal) getAuthentication().getPrincipal();
    }

    /**
     * Returns the authorities for the authenticated user.
     *
     * @return the authorities for the authenticated user
     */
    protected static Collection<? extends GrantedAuthority> getAuthorities() {
    return getAuthentication().getAuthorities();
    }

    protected static Authentication getAuthentication() {
    return SecurityContextHolder.getContext().getAuthentication();
    }
}
