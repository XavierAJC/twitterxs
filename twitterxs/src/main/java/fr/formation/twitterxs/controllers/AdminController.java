package fr.formation.twitterxs.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminController extends BaseController {
	
	@GetMapping("/template/{className}")
    protected Object template(@PathVariable("className") String className)
        throws Exception {
    return Class.forName(className).newInstance();
    }

}
