package fr.formation.twitterxs.controllers;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import fr.formation.twitterxs.dto.TestDto;
import fr.formation.twitterxs.services.TestService;

@RestController // Spring context
@RequestMapping("/test")  //1er niveau : classe
public class TestController {
	
	@Autowired //Soit on met @autowired sur le champ soit on le met sur le constructeur (bonne pratique)
	//@Quelifier("Nom_de_la_classe_d_implementation")
	private TestService service;
	
//	@Autowired
//	@Qualifier("")
//	public TestController(TestService service, TestService service2) {
		//service != null
//		this.service = service;
//		this.service = service2;
//	}
	
	@GetMapping("/hello") //2e niveau: method
	protected String hello() {
		return "{\"value\":\"Hello Spring boot!\"}";
	}
	
	@GetMapping("/weekdays")
	protected List<String> weekdays() {
		return Arrays.asList("Monday", "Tuesday", "Wednesday", "Thirsday", "Friday");
	}
	
	@GetMapping("/weekday/{day}") //PathVariable
	protected String weekday(@PathVariable ("day") String day) {
		
		String result = null;
		switch (day) {
		case "mo":
			result = "Monday";
			break;
		case "tu":
			result = "Tuesday";
			break;
			default:
				result = "Friday";
				break;
		}
		return result;
	}
	
	@GetMapping("/weekday")
	protected String weekdayParam(@RequestParam("abbr") String abbr) {
		return service.getWeekday(abbr);
	}

	@PostMapping("/save")
	protected TestDto saveWeekday(@RequestBody TestDto dto) {
		System.out.println(dto);
		return dto;
	}
}
