package fr.formation.twitterxs.controllers;

import java.util.List;

import javax.naming.directory.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.twitterxs.dto.SearchResultDto;
import fr.formation.twitterxs.dto.TweetCreateDto;
import fr.formation.twitterxs.dto.TweetDto;
import fr.formation.twitterxs.dto.TweetSearchDto;
import fr.formation.twitterxs.services.TweetService;

@RestController
@RequestMapping("/tweet")
public class TweetController extends BaseController {
	
	@Autowired
	private TweetService tweetService;
	
	@PostMapping("/create")
	protected void create( @RequestBody TweetCreateDto dto) {
		tweetService.create(dto);
	}
	
	@GetMapping("/feed/{username}")
	protected SearchResultDto<TweetDto> feed(
			@PathVariable(name = "username", required = false) String username,
			@RequestParam(name = "p", required = false) Integer p,
			@RequestParam(name = "s", required = false) Integer s) {
		int page = (null == p ? 0 : Integer.max(0, p)); //default page
		int size = (null == s ? 5 : Integer.max(1,  s)); // Default size
		
		TweetSearchDto dto = new TweetSearchDto(username, page, size);
		return tweetService.feed(dto);
	}


}
