package fr.formation.twitterxs.dto;

import java.time.LocalDate;

import javax.validation.Valid;
import javax.validation.constraints.*;

import org.springframework.format.annotation.DateTimeFormat;

import fr.formation.twitterxs.errors.Adult;
import fr.formation.twitterxs.errors.UniqueEmail;

public class UserCreateDto {
	
//	private Integer id;
	@Size(max=100, message= "{E_MAX_LENGTH_EXCEEDED}")
	@NotBlank(message= "{E_NOT_EMPTY}")
	private String firstname;
	@Size(max=100, message= "{E_MAX_LENGTH_EXCEEDED}")
	@NotBlank(message= "{E_NOT_EMPTY}")
	private String lastname;
	@Size(max=255, message= "{E_MAX_LENGTH_EXCEEDED}")
	@NotBlank(message= "{E_NOT_EMPTY}")
	@Email(message= "{E_EMAIL_MALFORMED}")
	@UniqueEmail
	private String email;
	@NotNull(message= "{E_NOT_NULL}")
	@Adult
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)  // convertit une chaine de caractere en date et permet de bien renvoyer une date dans le formulaire
	private LocalDate birthDate;
	private Integer regionId;
	@Valid //cascade validation
	private UserSecurityCreateDto userSecurity;
	
	public UserCreateDto() {
		super();
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public Integer getRegionId() {
		return regionId;
	}

	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

//	public Integer getId() {
//		return id;
//	}
//
//	public void setId(Integer id) {
//		this.id = id;
//	}

	public UserSecurityCreateDto getUserSecurity() {
		return userSecurity;
	}

	public void setUserSecurity(UserSecurityCreateDto userSecurity) {
		this.userSecurity = userSecurity;
	}

	@Override
	public String toString() {
		return "UserCreateDto [firstname=" + firstname + ", lastname=" + lastname + ", email=" + email + ", birthDate="
				+ birthDate + ", regionId=" + regionId + "]";
	}
	
	

}
