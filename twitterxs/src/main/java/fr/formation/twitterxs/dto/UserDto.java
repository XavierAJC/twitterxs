package fr.formation.twitterxs.dto;


public class UserDto {

	private Integer id;
	private String firstname;
	private String lastname;
	private String regionCountry;
	
	//Constructor
	public UserDto() {
		super();
	}

	public UserDto(Integer id, String firstname, String lastname, String country) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.regionCountry = country;
	}
	// Getters
	public Integer getId() {
		return id;
	}


	public String getFirstname() {
		return firstname;
	}


	public String getLastname() {
		return lastname;
	}


	public String getRegionCountry() {
		return regionCountry;
	}

	// Setters
	public void setId(Integer id) {
		this.id = id;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setRegionCountry(String regionCountry) {
		this.regionCountry = regionCountry;
	}

	
	
	
}
