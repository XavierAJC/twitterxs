package fr.formation.twitterxs.dto;

import java.util.List;

public class SearchResultDto<E> {
	
	private List<E> items;
	private long totalElements;
	
	public SearchResultDto() {
		super();
	}

	public SearchResultDto(List<E> items, long totalElements) {
		super();
		this.items = items;
		this.totalElements = totalElements;
	}

	public List<E> getItems() {
		return items;
	}

	public void setItems(List<E> items) {
		this.items = items;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}
	
	

}
