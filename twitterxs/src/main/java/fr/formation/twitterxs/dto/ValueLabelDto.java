package fr.formation.twitterxs.dto;

public class ValueLabelDto {

	private Object value;
	private String label;
	
	public ValueLabelDto() {
		super();
	}
	public ValueLabelDto(Object value, String label) {
		super();
		setValue(value);
		setLabel(label);
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	/**
     * Returns a string representation of this {@code dto}.
     *
     * @return a string representation of this {@code dto}
     */
    @Override
    public String toString() {
	return "{value=" + value + ", label=" + label + "}";
    }
	
}
