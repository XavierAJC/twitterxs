package fr.formation.twitterxs.dto;

import java.time.LocalDateTime;

public class TweetDto {
	
	private Integer tweetId;
	private LocalDateTime postDate;
	private String content;
	
	public TweetDto() {
		super();
	}

	public TweetDto(Integer tweetId, LocalDateTime postDate, String content) {
		super();
		this.tweetId = tweetId;
		this.postDate = postDate;
		this.content = content;
	}

	public Integer getTweetId() {
		return tweetId;
	}

	public void setTweetId(Integer tweetId) {
		this.tweetId = tweetId;
	}

	public LocalDateTime getPostDate() {
		return postDate;
	}

	public void setPostDate(LocalDateTime postDate) {
		this.postDate = postDate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	

}
