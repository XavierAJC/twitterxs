package fr.formation.twitterxs.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TweetCreateDto {
	
	@NotNull(message="{E_NOT_NULL}")
	private Integer authorId;
	@Size(max=280, message= "{E_MAX_LENGTH_EXCEEDED}")
	@NotBlank(message= "{E_NOT_EMPTY}")
	private String content;
	
	public TweetCreateDto() {
		super();
	}
	public Integer getAuthorId() {
		return authorId;
	}
	public void setAuthorId(Integer authorId) {
		this.authorId = authorId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	

}
