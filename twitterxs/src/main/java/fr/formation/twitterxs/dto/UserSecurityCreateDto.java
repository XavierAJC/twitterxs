package fr.formation.twitterxs.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserSecurityCreateDto {
	
	@Size(max=50, message= "{E_MAX_LENGTH_EXCEEDED}")
	@NotBlank(message= "{E_NOT_EMPTY}")
	private String username;
	
	@Size(max=10, message= "{E_MAX_LENGTH_EXCEEDED}")
	@NotBlank(message= "{E_NOT_EMPTY}")
	private String password;

	public UserSecurityCreateDto() {
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "UserSecurityCreateDto [username=" + username + "]";
	}
	
	

}
