package fr.formation.twitterxs.dto;

public class TestDto {
	private String abbr;

	public String getAbbr() {
		return abbr;
	}

	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}

	public TestDto() {
		super();
	}

	@Override
	public String toString() {
		return "TestDto [abbr=" + abbr + "]";
	}

}
