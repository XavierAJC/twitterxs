package fr.formation.twitterxs;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;


@SpringBootApplication
public class TwitterxsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TwitterxsApplication.class, args);
	}
	
	@Bean
	public ModelMapper mapper() {
		ModelMapper mapper = new ModelMapper();
//		mapper.addConverter(null); //change conf
		return mapper;
	}
	
	@Bean
    protected LocalValidatorFactoryBean validator(MessageSource messageSource) {
	    LocalValidatorFactoryBean validatorFactoryBean = new LocalValidatorFactoryBean();
	    validatorFactoryBean.setValidationMessageSource(messageSource);
	    return validatorFactoryBean;
   }
	
	// Obligatoire pour préciser à Spring qu'on utilise le meme encoder pour
	@Bean
	public PasswordEncoder passworEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	// Classe interne de config de la securité
	@EnableWebSecurity
	@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
	protected static class SecurityConfig extends WebSecurityConfigurerAdapter {
		
		@Autowired
		private UserDetailsService userDetailsService;
		
		@Override
		protected void configure (AuthenticationManagerBuilder auth)
				throws Exception {
			auth.userDetailsService(userDetailsService);
		}
		
		// Permet de configurer la sécurité, plein de possibilités dans HttpSecurity
		@Override
	    protected void configure(HttpSecurity http) throws Exception {
	        // CSRF disabled to ease tests with Postman
	        http.csrf().disable().authorizeRequests()
	            .antMatchers("/login", "/security/authError",
	                "/security/login", "/security/logout",
	                "/user/create", "/mvc/**") // /user/create permet d'avoir acces à la création de compte sans être authentifier
	            .permitAll()
	            .and()
	            .formLogin()
	            .loginPage("/security/login")
	            .loginProcessingUrl("/login")
	            .defaultSuccessUrl("/security/me", true)
	            .failureUrl("/security/authError")
	            .and()
	            .logout()
	            .invalidateHttpSession(true)
	            .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
	            .logoutSuccessUrl("/security/logout")
	            .and()
	            .authorizeRequests()
	            .anyRequest()
	            .authenticated()
	            .and()
	            .authorizeRequests()
	            .antMatchers("/actuator/**")
	            .hasRole("ACTUATOR"); //!\\ Sans le prefixe ROLE ou alors .hasAuthority("ROLE_ACTUATOR")
	    }
	}

}

