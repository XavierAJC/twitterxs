package fr.formation.twitterxs.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import fr.formation.twitterxs.entities.Region;
import fr.formation.twitterxs.repository.RegionRepository;

@Repository
public class RegionRepositoryImpl implements RegionRepository {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional
	public Integer save(Region region) {
		System.out.println("## Before persist : " + region.getId());
		em.persist(region);
		System.out.println("## After persist : " + region.getId());
		return region.getId();
	}

	

}
