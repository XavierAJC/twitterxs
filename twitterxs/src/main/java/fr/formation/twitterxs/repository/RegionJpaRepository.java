package fr.formation.twitterxs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.formation.twitterxs.dto.ValueLabelDto;
import fr.formation.twitterxs.entities.Region;

public interface RegionJpaRepository extends JpaRepository<Region, Integer> {
	
	@Query("select new fr.formation.twitterxs.dto.ValueLabelDto(r.id, r.language || '_' || r.country) " 
	 		+ "from Region r " 
	 		+ "order by r.language, r.country")
	public List<ValueLabelDto> findAllAsDto();

}
