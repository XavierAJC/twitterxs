package fr.formation.twitterxs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.Nullable;

import fr.formation.twitterxs.dto.UserDto;
import fr.formation.twitterxs.entities.User;

public interface UserJpaRepository extends JpaRepository<User, Integer> {

	 public List<UserDto> findAllByFirstnameContaining (String str);
	 
	 @Query("select new fr.formation.twitterxs.dto.UserDto(u.id, u.firstname, u.lastname, r.country) " //le nombre d'arguments doit matcher celui du construteur du dto
	 		+ "from User u " //En JPQL, on interroge la classe User et non pas la table user
	 		+ "join u.region r "
	 		+ "where u.firstname like %:chars%")
//	 		+ "or u.lastname like %:chars%")
	 public List<UserDto> findAllAsDto(@Param("chars") String chars);
	 
	 public boolean existsByEmailIgnoreCase (String email);
	 
	 public boolean existsByUserSecurityUsernameIgnoreCase(String username);
	 
	 @Nullable // Indicates that it can return an Optional i.e. it can be null
	 // select u from User u where u.userSecurity.username = :username => requete jpql
	 public User findByUserSecurityUsername(String username); // => requete dérivée
	 

}
