package fr.formation.twitterxs.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.formation.twitterxs.dto.TweetDto;
import fr.formation.twitterxs.entities.Tweet;

public interface TweetJpaRepository extends JpaRepository<Tweet, Integer> {
	
	@Query("select new fr.formation.twitterxs.dto.TweetDto(t.id, t.postDate, t.content) " 
	 		+ "from Tweet t " //En JPQL, on interroge la classe Tweet pour remplir le TweetDto et non pas la table tweet
	 		+ "where t.author.userSecurity.username = :username "
	 		+ "order by t.postDate desc")
	public Page<TweetDto> findByUsername(@Param("username") String username,
			Pageable pageable);

}
