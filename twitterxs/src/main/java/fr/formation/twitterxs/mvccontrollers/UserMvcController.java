package fr.formation.twitterxs.mvccontrollers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.formation.twitterxs.dto.UserCreateDto;
import fr.formation.twitterxs.dto.ValueLabelDto;
import fr.formation.twitterxs.services.RegionService;
import fr.formation.twitterxs.services.UserService;

/**
 * 
 * A MVC Controller to deal with users.
 *
 */
@Controller
@RequestMapping("/mvc/users")
public class UserMvcController {
	
	private final UserService userService;
	private final RegionService regionService;

	protected UserMvcController(UserService userService, RegionService regionService) {
		super();
		this.userService = userService;
		this.regionService = regionService;
	}
	
	@SuppressWarnings("unused")
	@GetMapping("/create")
	public String create(@ModelAttribute("user") UserCreateDto user, Model model) {
		populateModel(model); //permet de remplir la list des region
		return "user";
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("user") UserCreateDto user, 
			BindingResult result, Model model) {
		if (!result.hasErrors()) { // No annotation-like validation errors
			userService.create(user);
			model.addAttribute("user", new UserCreateDto()); // Reset form
		}
		populateModel(model);
		return "user";
	}
	
	private void populateModel(Model model) {
		List<ValueLabelDto> regions = regionService.findAll();
		model.addAttribute("regions", regions);
	}

}
