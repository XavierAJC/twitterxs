package fr.formation.twitterxs.mvccontrollers;

import javax.validation.Valid;

import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.formation.twitterxs.dto.SearchResultDto;
import fr.formation.twitterxs.dto.TweetCreateDto;
import fr.formation.twitterxs.dto.TweetDto;
import fr.formation.twitterxs.dto.TweetSearchDto;
import fr.formation.twitterxs.services.TweetService;

/**
 * 
 * A MVC Controller to deal with tweets.
 *
 */
@Controller
@RequestMapping("/mvc/tweets")
public class TweetMvcController {
	
	private final TweetService service;

	protected TweetMvcController(TweetService service) {
		super();
		this.service = service;
	}
	
	@GetMapping("/feed/{username}")
	public String feed (@PathVariable("username") String username, Model model) {
		TweetSearchDto dto = new TweetSearchDto(username, 0, 100);
		SearchResultDto<TweetDto> tweets = service.feed(dto);
		model.addAttribute("tweets", tweets);
		return "feed"; //On retourne une page jsp ou redirige vers un autre controller: redirect: "/..."
	}
	
	@SuppressWarnings("unused")
	@GetMapping("/create")
	public String create(@ModelAttribute("tweet") TweetCreateDto tweet, Model model) {
		return "create";
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("tweet") TweetCreateDto tweet, 
			BindingResult result, Model model) {
		if (!result.hasErrors()) { // No annotation-like validation errors
			service.create(tweet);
			model.addAttribute("tweet", new TweetCreateDto()); // Reset form
		}
		return "create";
	}

}
